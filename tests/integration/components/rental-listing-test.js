import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import Ember from 'ember';

let rental = Ember.Object.create({
  id: "fake-id",
  image: 'fake.png',
  title: 'test-title',
  owner: 'test-owner',
  type: 'test-type',
  city: 'test-city',
  bedrooms: 3
});

moduleForComponent('rental-listing', 'Integration | Component | rental listing', {
  integration: true
});

test('should display rental details', function(assert) {
  this.set('rentalObj', rental);
  this.render(hbs`{{rental-listing rental=rentalObj}}`);
  assert.equal(this.$('.listing-rentals h3').text(), 'test-title', 'Title: test-title');
  assert.equal(this.$('.listing-rentals .caption dl dd.owner').text().trim(), 'test-owner');
});

test('should toggle wide class on click', function(assert) {
  this.set('rentalObj', rental);
  this.render(hbs`{{rental-listing rental=rentalObj}}`);
  assert.equal(this.$('.larger-link.wide').length, 0, 'initially rendered small');
  this.$(".larger-link").click();
  assert.equal(this.$('.larger-link.wide').length, 1, 'rendered wide after click');
  this.$(".larger-link").click();
  assert.equal(this.$('.larger-link.wide').length, 0, 'rendered small after second click');
});

test('should toggle modal on click', function(assert) {
  this.set('rentalObj', rental);
  this.render(hbs`{{rental-listing rental=rentalObj}}`);
  this.$(".larger-link").click();
  assert.equal(this.$('.modal.wider-render-img-modal-' + rental.id).length, 1, 'rendered modal');
  assert.equal(this.$('.modal.wider-render-img-modal-' + rental.id).hasClass("wide"), true, 'rendered modal wided');
});

// test('it renders', function(assert) {
//
//   // Set any properties with this.set('myProperty', 'value');
//   // Handle any actions with this.on('myAction', function(val) { ... });
//
//   this.render(hbs`{{rental-listing}}`);
//
//   assert.equal(this.$().text().trim(), '');
//
//   // Template block usage:
//   this.render(hbs`
//     {{#rental-listing}}
//       template block text
//     {{/rental-listing}}
//   `);
//
//   assert.equal(this.$().text().trim(), 'template block text');
// });
