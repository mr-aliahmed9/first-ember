import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';
import wait from 'ember-test-helpers/wait';
import RSVP from 'rsvp';

const ITEMS = [{city: 'San Francisco', type: 'Apartment'}, {city: 'Portland', type: 'Condo'}, {city: 'Seattle', type: 'Estate'}, {city: 'New York', type: 'Estate'}];
// const FILTERED_CITY_ITEM = [{city: 'San Francisco'}];
// const FILTERED_TYPE_ITEM = [{city: 'Condo'}];
const FILTERED_ITEMS = [{city: 'San Francisco', type: 'Apartment'}];

moduleForComponent('list-filter', 'Integration | Component | list filter', {
  integration: true
});

test('should initially load all listings', function (assert) {
  // we want our actions to return promises,
  //since they are potentially fetching data asynchronously
  this.on('filterByInputs', (obj) => {
    if (Object.keys(obj).length == 0) {
      return RSVP.resolve(ITEMS);
    } else {
      return RSVP.resolve(FILTERED_ITEMS);
    }
  });
  // with an integration test,
  // you can set up and use your component in the same way your application
  // will use it.
  this.render(hbs`
    {{#list-filter filter=(action 'filterByInputs') as |results|}}
      <ul>
      {{#each results as |item|}}
        <li class="city">
          {{item.city}}
        </li>
        <li class="type">
          {{item.type}}
        </li>
      {{/each}}
      </ul>
    {{/list-filter}}
  `);

  return wait().then(() => {
    assert.equal(this.$('.city').length, 4);
    assert.equal(this.$('.city').first().text().trim(), 'San Francisco');
    assert.equal(this.$('.type').length, 4);
    assert.equal(this.$('.type').first().text().trim(), 'Apartment');
  });
});

test('should update with matching listings', function (assert) {
  // we want our actions to return promises,
  //since they are potentially fetching data asynchronously
  this.on('filterByInputs', (obj) => {
    if (Object.keys(obj).length == 0) {
      return RSVP.resolve(ITEMS);
    } else {
      return RSVP.resolve(FILTERED_ITEMS);
    }
  });
  // with an integration test,
  // you can set up and use your component in the same way your application
  // will use it.
  this.render(hbs`
    {{#list-filter filter=(action 'filterByInputs') as |results|}}
      <ul>
      {{#each results as |item|}}
        <li class="city">
          {{item.city}}
        </li>
        <li class="type">
          {{item.type}}
        </li>
      {{/each}}
      </ul>
    {{/list-filter}}
  `);

  // The keyup event here should invoke an action that will cause the list to be filtered
  this.$('.list-filter input').val('San').keyup();

  return wait().then(() => {
    assert.equal(this.$('.city').length, 1);
    assert.equal(this.$('.city').text().trim(), 'San Francisco');
  });
});
