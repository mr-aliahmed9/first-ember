import { test } from 'qunit';
import moduleForAcceptance from 'first-ember/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | list rentals');

test('visiting /', function(assert) {
  visit('/');

  andThen(function() {
    assert.equal(currentURL(), '/');
  });
});

// test('should show rentals as the home page', function (assert) {
// });
//
// test('should link to information about the company.', function (assert) {
// });
//
// test('should link to contact information.', function (assert) {
// });

test('should list available rentals.', function (assert) {
  visit('/');
  andThen(function() {
    assert.equal(find('.listing-rentals').length, 4, 'should see 4 listings');
  });
});

test('should filter the list of rentals by city.', function (assert) {
  visit('/');
  fillIn('.list-filter input', 'Seattle');
  keyEvent('.list-filter input', 'keyup', 69);
  andThen(function() {
    assert.equal(find('.listing-rentals').length, 1, 'should show 1 listing');
    assert.equal(find('.listing-rentals .location:contains("Seattle")').length, 1, 'should contain 1 listing with location Seattle');
  });
});

test('should filter the list of rentals by type.', function (assert) {
  visit('/');
  fillIn('.list-filter select', 'Condo');
  andThen(function() {
    assert.equal(find('.listing-rentals').length, 1, 'should show 1 listing');
    assert.equal(find('.listing-rentals .type:contains("Condo")').length, 1, 'should contain 1 listing with type Condo');
  });
});
//
// test('should show details for a selected rental', function (assert) {
// });
