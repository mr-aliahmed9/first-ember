import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    filterByInputs(params) {
      if (Object.keys(params).length != 0) {
        return this.get('store').query('rental', params);
      } else {
        return this.get('store').findAll('rental');
      }
    }
  }
});
