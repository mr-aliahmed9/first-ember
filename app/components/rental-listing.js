import WiderRentalImageComponent from './wider-rental-image';

export default WiderRentalImageComponent.extend({
  isWide: false,
  actions: {
    toggleImageSize(id) {
      this.toggleProperty('isWide');
      this.$(".wider-render-img-modal-" + id).modal("show");
    },
    toggleMap(id) {
      this.$(".rental-map-modal-" + id).modal("show");
    }
  }
});
