import Ember from 'ember';

export default Ember.Component.extend({
  rentalTypes: ['Estate', 'Apartment', 'Condo'],
  classNames: ['list-filter'],
  value: '',
  filterParams: {},

  init() {
    this._super(...arguments);
    this.get('filter')({}).then((results) => this.set('results', results));
  },

  actions: {
    handleFilterEntry(paramType, value) {
      let filterInputValue = value;
      let filterAction = this.get('filter');
      let filterParams = this.get('filterParams');
      if(!filterInputValue) {
        filterInputValue = this.get('value');
      }
      filterParams[paramType] = filterInputValue;
      this.set('filterParams', filterParams);
      for(var key in filterParams) {
        if (filterParams.hasOwnProperty(key)) {
          var val = filterParams[key];
          if(val === '') {
            delete filterParams[key];
          }
        }
      }

      filterAction(filterParams).then((filterResults) => this.set('results', filterResults));
    },
    selectFilterHelper(paramType) {
      var value = this.$('option:selected').val();
      this.send('handleFilterEntry', paramType, value);
    }
  }
});
